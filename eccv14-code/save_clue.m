function save_clue(color_image, depth_image, C, colorModel, cacheFilePath)
    % Save the preprocessed data for contour computation
    %
    % Input:
    % color_image: color image (in matrix)
    % depth_image: depth image (in matrix)
    % C: camera matrix
    % colorModel: used for contour detection
    % cacheFilePath: path to save the preprocessed data
    
    vars = {'ng1', 'ng2', 'Es', 'y1', 'y2', 'y3', 'angl1', 'angl2'};
    getAllCues(color_image, depth_image, C, colorModel, vars, cacheFilePath);
end