function rec = getGroundTruthBoxes_UW_for_test(imdb, ii)
    if ~isfield(imdb, 'rec')
        filename = imdb.test_data_info;
        % filename = ['/home/tom/object_slam/object_detection/UW_complete_test', '.mat'];
        rec = load(filename);
        imdb.rec = rec.gt_data;
    end
    imname = rec.gt_data(ii).fname;
    rec = rec.gt_data(ii);
    rec.img = imname;
end
