function HHA = getHHA_from_file(depth_filename, rawdepth_filename, C, outDir)
    % Generate HHA image from depth image
    %
    % Input:
    % depth_filename: the path of one single *processed* depth image
    % rawdepth_filename: the path of unprocessed depth image
    % C: camera matrix
    % outDir: the path to save

    D = imread(depth_filename);
    RD = imread(rawdepth_filename);
    D = double(D)./1000;
%     missingMask = ones(size(D)) == 1;
    missingMask = RD == 0;
    [pc, N, yDir, h, pcRot, NRot] = processDepthImage(D*100, missingMask, C);
    angl = acosd(min(1,max(-1,sum(bsxfun(@times, N, reshape(yDir, 1, 1, 3)), 3))));
    
    % Making the minimum depth to be 100, to prevent large values for disparity!!!
    pc(:,:,3) = max(pc(:,:,3), 100); 
    I(:,:,1) = 31000./pc(:,:,3); 
    I(:,:,2) = h;
    I(:,:,3) = (angl+128-90); %Keeping some slack
    I = uint8(I);
%     % Save if can save
%     if(~isempty(outDir) && ~isempty(imName)), imwrite(I, fullfile_ext(outDir, imName, 'png')); end

    HHA = I;
end