function output = UW_extractor(start_ind, end_ind)

% =================================
% Modify this two lines
dataset_dir = '/home/tom/rgbd-dataset';
target_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo';
% =================================

class_folders = dir(dataset_dir);
class_folders = {class_folders.name};
class_folders = class_folders(3:end);

C = [525 0 320; ...
    0 525 240; ...
    0 0 1]; % instrinsic matrix used in UW rgbd scene dataset

for folder_ind = start_ind:end_ind
    disp('current frame')
    disp(folder_ind)
    disp('current class')
    disp(class_folders{folder_ind})
    class_dir = fullfile(dataset_dir, class_folders{folder_ind});
    class_content_fnames = dir(class_dir);
    class_content_fnames = {class_content_fnames.name};
    class_content_fnames = class_content_fnames(3:end);
    for file_ind = 1:length(class_content_fnames)
        file_name = class_content_fnames{file_ind};
        start_pt = regexp(file_name, '_depthcrop.png', 'start');
        if length(start_pt) == 0
            continue
        end
        rawdepth_name = [file_name(1:start_pt(1) - 1), '.png'];
        rawdepth_dir = fullfile(target_dir, 'rawdepth', class_folders{folder_ind});
        if ~exist(rawdepth_dir)
            mkdir(rawdepth_dir)
        end
        if ~exist(fullfile(rawdepth_dir, rawdepth_name))
            copyfile(fullfile(class_dir, file_name), fullfile(rawdepth_dir, rawdepth_name));
        end
        
        rgb_name = [file_name(1:start_pt(1) - 1), '_crop.png'];
        
        
        rgb_dir = fullfile(target_dir, 'rgb', class_folders{folder_ind});
        if ~exist(rgb_dir)
            mkdir(rgb_dir)
        end
        if ~exist(fullfile(rgb_dir, rgb_name))
            rgb_img = imread(fullfile(class_dir, rgb_name));
            imwrite(imresize(rgb_img, [256, 256]), fullfile(rgb_dir, rawdepth_name))
            % copyfile(fullfile(class_dir, rgb_name), fullfile(rgb_dir, rawdepth_name));
        end
        
        
        
        depth_dir = fullfile(target_dir, 'depth', class_folders{folder_ind});
        if ~exist(depth_dir)
            mkdir(depth_dir)
        end
        if ~exist(fullfile(depth_dir, rawdepth_name))
            RD = imread(fullfile(class_dir, file_name));
            rgb_img = imread(fullfile(class_dir, rgb_name));
            D = fill_depth_colorization(double(rgb_img) ./ 255, double(RD) ./ 1000);
            D = uint16(D * 1000);

            imwrite(D, fullfile(depth_dir, rawdepth_name));
        end

        hha_dir = fullfile(target_dir, 'hha', class_folders{folder_ind});
        if ~exist(hha_dir)
            mkdir(hha_dir)
        end
        if ~exist(fullfile(hha_dir, rawdepth_name))
            HHA = getHHA_from_file(fullfile(depth_dir, rawdepth_name), fullfile(rawdepth_dir, rawdepth_name), C, 0);
            HHA = imresize(HHA, [256,256]);
            imwrite(HHA, fullfile(hha_dir, rawdepth_name));
        end

    end
end
    
    output = folder_ind;
end