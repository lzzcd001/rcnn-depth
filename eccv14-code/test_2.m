load('/home/tom/Desktop/out.mat');
len = size(candidates.bboxes, 1)
  boxes = candidates.bboxes(1:min(2000, len), [2 1 4 3]);

% Visualize some detections
  cls_id = [6, 9, 12, 19, 20];
  cols = lines(length(cls_id));
  Idet = I;
  for i = 1:length(cls_id),
%     dt = load(fullfile(conf.cache_dir, 'pr-curves', sprintf('%s_pr_nyud2_test_release.mat', cls{cls_id(i)})));
%     dt = load('/home/tom/rcnn-depth/eccv14-cachedir/release/detection/detector/rgb_hha_30000_train/pr-curves/%s_pr_UW_test_release.mat', cls{num2str(cls_id)});
size(boxes)
size(detection_scores_no_nms(:,cls_id(i)))
    bbox = cat(2, boxes, detection_scores_no_nms(:,cls_id(i)));
    keep = false(size(bbox(:,1)));
    keep(rcnn_nms(bbox, 0.3)) = 1;
%     thresh = dt.thresh(find(dt.prec > 0.8, 1, 'last'));
    
    ind = bbox(:,5) > -.7;
    keep = find(keep & ind);
    bbox = bbox(keep,:);
    if(size(bbox,1) > 0)
      Idet = draw_rect_vec(Idet, bbox(:,1:4)', im2uint8(cols(i,:)), 2);
    end
  end
  figure(1); subplot(1,2,1); imagesc(Idet); axis image; title(['detections - ', sprintf('%s, ', cls{cls_id})]);
  figure(1); subplot(1,2,2); plot([1:length(cls_id)], 1); legend(cls(cls_id)); axis image;
  