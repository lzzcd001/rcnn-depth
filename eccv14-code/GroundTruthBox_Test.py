import os
import re
import cv2
import json
import scipy.misc
import numpy as np
import matlab.engine
from collections import Counter


def ground_truth_oneScene(scene_root_path, scene_paths, object_list_file, k):

	# Regex to parse the label name
	r = re.compile('[a-z_]+')

	# get this scene folder
	scenes = [os.path.join(scene_root_path, one_scene_path) for one_scene_path in scene_paths]

	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]

	output_image_number = 0
	test_count = 0
	gt_data_list = []
	for kk, scene in enumerate(scenes):
		json_path = os.path.join(scene, 'annotation/index.json') # Annotation information file
		with open(json_path, 'r') as annotation_file:
			frameCounter = Counter() # To deternune whether a frame is already used
			# Get what kinds of objects appear in this scene
			data = json.load(annotation_file)
			existing_objects = [ r.search(data['objects'][i]['name']).group() for i in xrange(len(data['objects'])) if data['objects'][i] != None ]
			miss = len(data['objects']) - len(existing_objects)

			total_count = k
			# Run through every labeled frame to extract cropped images and to save corresponding label into file
			for i in xrange(len(data['frames'])):
				# print 'this is ', i	
				# print data['frames'][i].keys()
				if len(data['frames'][i].keys()) != 0 and frameCounter[i] == 0:
					for k in xrange(np.max([i - 10, 0]), np.min([i+11, len(data['frames']) ])):
						filename_stem = str(scene_paths[kk]) + '/' + ('%.5d' % k)
						image_file_name = ('%.5d' % k) + '-color.png'
						image_file_name_depth = ('%.5d' % k) + '-depth.png'
						image_path = os.path.join(scene, image_file_name)
						image_path_depth = os.path.join(scene, image_file_name_depth)
						if not (frameCounter[k] == 0 and os.path.exists(image_path)):
							continue
						else:
							frameCounter[k] += 1
							total_count += 1
						# Run through every labeled object in this scene
						objects = []
						for j in xrange(len(data['frames'][i]['polygon'])):
							obj_index = data['frames'][i]['polygon'][j]['object']
							obj_number = object_name_to_order[existing_objects[obj_index - miss]]

							# Get bounding box of objects
							left_x = min(data['frames'][i]['polygon'][j]['x'])
							right_x = max(data['frames'][i]['polygon'][j]['x'])
							left_y = min(data['frames'][i]['polygon'][j]['y'])
							right_y = max(data['frames'][i]['polygon'][j]['y'])

							if right_x <= left_x or right_y <= left_y:
								continue

							substruct = {'class': str(obj_number + 1), 
										'bbox': [left_x, left_y, right_x, right_y],
										'instanceId': 1,
										'difficult': 0,
										'truncated': 0
										}
							objects.append(substruct)
						if objects == []:
							total_count -= 1
							continue
						test_count += 1
						current_struct = {'objects': objects,
										'imgsize': [640, 480],
										'frame_no': '%.5d' % total_count,
										'fname': filename_stem}
						# gt_data[str(total_count)] = current_struct
						gt_data_list.append(current_struct)
	return gt_data_list

if __name__ == '__main__':
  # ===================================================
  # Modify these lines
	object_list_file = '/home/tom/rgbd-dataset/uwLabels.txt'
	dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo'
	scene_root_path = '/home/tom/rgbd-scenes-v2/imgs/'
  # ===================================================


	number_train_to = 3
	scene_paths = ['scene_' + ('%.2d' % k) for k in xrange(number_train_to, 15)]
	flag = 'test'
	savename = 'UW_complete_' + flag + '.json'
	list_1 = ground_truth_oneScene(scene_root_path, scene_paths, object_list_file, 0)
	with open(savename, 'w') as savefile:
		json.dump(list_1, savefile)


	print "Finished saving JSON"
	eng = matlab.engine.start_matlab()
	eng.addpath('./matlab-json-master')
	eng.jsonToMat(savename, 'UW_' + flag + '.mat')

