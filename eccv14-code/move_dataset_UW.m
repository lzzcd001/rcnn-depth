dataset_dir = '/home/tom/rgbd-dataset';
target_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo';
folder_names = {'depth', 'rawdepth'};

class_folders = dir(dataset_dir);
class_folders = {class_folders.name};
class_folders = class_folders(3:end);
for folder_ind = 1:length(class_folders)
    disp('current frame')
    disp(folder_ind)
    disp('current class')
    disp(class_folders{folder_ind})
    class_dir = fullfile(dataset_dir, class_folders{folder_ind});
    class_content_fnames = dir(class_dir);
    class_content_fnames = {class_content_fnames.name};
    class_content_fnames = class_content_fnames(3:end);
    for file_ind = 1:length(class_content_fnames)
        file_name = class_content_fnames{file_ind};
        start_pt = regexp(file_name, '_depthcrop', 'start');
        if length(start_pt) == 0
            continue
        end
        rawdepth_name = [file_name(1:start_pt(1) - 1), '.png'];
        if ~exist(fullfile(target_dir, 'rawdepth', class_folders{folder_ind}))
            mkdir(fullfile(target_dir, 'rawdepth', class_folders{folder_ind}))
        end
        copyfile(fullfile(class_dir, file_name), fullfile(target_dir, 'rawdepth', class_folders{folder_ind}, rawdepth_name));
        
        rgb_name = [file_name(1:start_pt(1) - 1), '_crop.png'];
        RD = imread(fullfile(class_dir, file_name));
        rgb_img = imread(fullfile(class_dir, rgb_name));
        D = fill_depth_colorization(double(rgb_img) ./ 255, double(RD) ./ 1000);
        D = uint16(D * 1000);
        
        
        if ~exist(fullfile(target_dir, 'depth', class_folders{folder_ind}))
            mkdir(fullfile(target_dir, 'depth', class_folders{folder_ind}))
        end
        imwrite(D, fullfile(target_dir, 'depth', class_folders{folder_ind}, rawdepth_name));
    end
end