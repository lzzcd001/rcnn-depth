import os
import re
import cv2
import json
import scipy.misc
import numpy as np
import matlab.engine
from collections import Counter

def ground_truth_UW(savename, object_raw_list, dataset_filename):

	# Regex to parse the label name
	r = re.compile('[a-z_]+/[a-z_]+_[0-9]+_[0-9]+')

	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]


	gt_data_list = []
	with open(dataset_filename, 'r') as dataset_file:
		content = map(lambda x: x.rstrip().split(' '), dataset_file.readlines())
		# random_inds = np.random.choice(len(content), int(len(content) * 0.1), replace=False)
		# content = content[random_inds]
		content = content[::100]
		filename_list = map(lambda x: x[0], content)
		class_list = map(lambda x: x[1], content)
		for ind, filename in enumerate(filename_list):
			tmp_img = cv2.imread(filename)
			right_x, right_y = tmp_img.shape[:2]

			# Get bounding box of objects
			left_x = 1
			left_y = 1

			try:
				filename_start_ind = re.search(r, filename).start()
			except:
				print
				print filename
				print
				raise
			#### Assuming the file extension is PNG
			filename_stem = filename[filename_start_ind: -4]

			substruct = {'class': str(int(class_list[ind]) + 1), 
						'bbox': [left_x, left_y, right_x, right_y],
						'instanceId': 1,
						'difficult': 0,
						'truncated': 0
						}
			objects = [substruct]
			# objects.append(substruct)
			current_struct = {'objects': objects,
							'imgsize': [right_y, right_x],
							'frame_no': '%.5d' % ind,
							'fname': filename_stem}

			gt_data_list.append(current_struct)
		with open(savename, 'w') as json_writefile:
			json.dump(gt_data_list,  json_writefile)


if __name__ == '__main__':
  # ===================================================
  # Modify these lines
	object_list_file = '/home/tom/rgbd-dataset/uwLabels.txt'
	dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo']
	scene_root_path = '/home/tom/rgbd-scenes-v2/imgs/'
  # ===================================================

	for flag in ['train', 'test']:
	  savename = 'UW_' + flag + '.json'
	  ground_truth_UW(savename, object_list_file, os.path.join(dataset_dir, flag + '_rgb.txt'))
	  print "Finished saving JSON"
	  eng = matlab.engine.start_matlab()
	  eng.addpath('./matlab-json-master')
	  eng.jsonToMat(savename, 'UW_' + flag + '.mat')

