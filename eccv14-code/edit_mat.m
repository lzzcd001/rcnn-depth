function edit_mat(filename)
    gt_data = load(filename);
    gt_data = gt_data.gt_data;
    for ii = 1:length(gt_data)
        rec_ii = gt_data(ii);
        if ~iscell(rec_ii.objects)
            tmp = cell(1, length(rec_ii.objects));
            for jj = 1:length(rec_ii.objects)
                tmp{jj} = rec_ii.objects(jj);
            end
            gt_data(ii).objects = tmp;
        end
    end
    save(filename, 'gt_data');
end
