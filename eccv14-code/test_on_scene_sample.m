I = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_scene/scene_14/rgb/00000-color.png');
RD = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_scene/scene_14/rawdepth/00000.png');
D = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_scene/scene_14/depth/00000.png');

% I = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_loo/rgb/apple/apple_1_1_1.png');
% RD = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_loo/rawdepth/apple/apple_1_1_1.png');
% D = imread('/home/tom/object_slam_datasets/rcnn_hha_dataset_loo/depth/apple/apple_1_1_1.png');
size(I)
size(RD)
size(D)
I = imresize(I, size(D));
size(I)
C = [525 0 320; ...
    0 525 240; ...
    0 0 1]; % instrinsic matrix used in UW rgbd scene dataset
out_file = '/home/tom/Desktop/out.mat';
[E, ucm2, candidates, detection_scores_no_nms, cls] = run_all_UW(I, D, RD, C, out_file)
detection_scores_no_nms