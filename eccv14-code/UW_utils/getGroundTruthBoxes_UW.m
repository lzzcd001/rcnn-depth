function rec = getGroundTruthBoxes_UW(filename, ii)
    rec = load(filename);
    rec = rec.gt_data(ii);
end
