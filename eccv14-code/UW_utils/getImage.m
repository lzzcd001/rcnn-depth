function I = getImage(dataDir, imName, typ)
% function I = getImage(dataDir, imName, typ)
%  Input:
%   typ   one of ['images', 'depth']

% AUTORIGHTS

  I = imread(fullfile(dataDir, typ, strcat(imName, '.png')));
end
