function imdb = imdb_from_UW(root_dir, code_root_dir, imset, task, regionDir, salt, max_boxes, roidb)
  
  if nargin == 7
      imdb.roidb = roidb;
  end
  
  % Load the image sets
  [imList] = getImageSet_UW(root_dir, imset);
  imdb.image_ids = imList;
  imdb.fold = ones(size(imdb.image_ids));
  imdb.root_dir = code_root_dir;
  imdb.max_boxes = 200;
  % dt = load(fullfile(root_dir, 'splits-rmrc.mat'), imset);
  % imlist = dt.(imset);
  % for i = 1:length(dt.(imset)),
  %   imdb.image_ids{i} = sprintf('img_%04d', imlist(i));
  %   imdb.fold(i) = 1;
  % end
  imdb.imset = imset;

  % Load the class mapping
  cls_to_id = containers.Map();
%   for i = 1:length(imdb.task.name),
%       cls_to_id(sprintf('%d', i)) = i;
%   end

  for i = 1:51,
      cls_to_id(sprintf('%d', i)) = i;
  end

  imdb.cls_to_id = cls_to_id;
  classes = cell(1,51);
  for i=1:51
      classes{i} = num2str(i);
  end
  imdb.classes = classes;
  imdb.num_classes = 51;
  imdb.class_ids = 1:imdb.num_classes;

  imdb.name = ['UW_' imset '_' salt];
  imdb.dataset_name = ['UW_' salt];

  imdb.image_dir = fullfile(root_dir, 'rgb');
  imdb.depth_dir = fullfile(root_dir, 'depth');
  imdb.rawdepth_dir = fullfile(root_dir, 'rawdepth');

  imdb.image_ext = 'png';
  imdb.depth_ext = 'png';
  imdb.rawdepth_ext = 'png';


  imdb.regionDir = regionDir;
  imdb.roi_func = @(x, y) roi_from_nyud2(x, y);
  imdb.eval_func = @imdb_eval_UW;

  imdb.max_boxes = max_boxes;
end
