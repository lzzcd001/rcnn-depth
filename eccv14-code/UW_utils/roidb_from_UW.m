function roidb = roidb_from_UW(imdb)

  imdb_name = imdb.name;
  image_ids = imdb.image_ids;
  cls_to_id = imdb.cls_to_id;
  num_classes = imdb.num_classes;
    prev_rng = rcnn_seed_rand();
   if isfield(imdb, 'roidb')
       roidb = imdb.roidb;
       return
   end
%   edit_mat(['/home/tom/object_slam/object_detection/UW_complete_', imdb.imset,  '.mat']); 
parfor i = 1:length(image_ids)
    tic_toc_print('roidb (%s): %d/%d\n', imdb_name, i, length(image_ids));
    
    % Load the ground truth annotations
    rec = getGroundTruthBoxes_UW(fullfile(imdb.root_dir, ['UW_', imdb.imset,  '.mat']), i); 
    boxes = zeros(imdb.max_boxes, 4);
    
    % Load random boxes
    if ~isfield(imdb, 'roidb')
      for kk=1:imdb.max_boxes
        xs = randi([1,rec.imgsize(1)],2);
        while xs(1) >= xs(2)
            xs = randi([1,rec.imgsize(1)],2);
        end
        ys = randi([1,rec.imgsize(2)],2);
        while ys(1) >= ys(2)
            ys = randi([1,rec.imgsize(2)],2);
        end
        boxes(kk, :) = [xs(1),ys(1), xs(2), ys(2)];
      end
    else
        boxes(:,:) = imdb.roidb.rois(i).boxes(2:end, :);
  end
    
%     dt = load(fullfile_ext(regionDir, image_ids{i}, 'mat'), 'bboxes');
%     dt.bboxes = dt.bboxes(1:min(imdb.max_boxes, size(dt.bboxes,1)), [2 1 4 3]);
    % Attach the regions
    rois(i) = attach_proposals(rec, boxes, cls_to_id, num_classes);
  end
   
  roidb.rois = rois;
  
  % restore previous rng
  rng(prev_rng);
end
