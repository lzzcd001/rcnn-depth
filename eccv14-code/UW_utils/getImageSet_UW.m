function [file_dirs] = getImageSet_UW(root_dir, imset)

if strcmp(imset, 'all')
    imset = {'train', 'test'};
else
    imset = {imset};
end
file_dirs = {};
for tag = imset
    dataset_filename = fullfile(root_dir, [tag{:} '.mat']);
    gt_data = load(dataset_filename);
    file_dirs = [file_dirs {gt_data.gt_data(:).fname}];
end


% 
%     if root_dir(end) ~= '/'
%         root_dir = [root_dir, '/'];
%     end
% 
%     data_mat_file = fullfile(root_dir, 'datadir_label.mat');
%     if exist(data_mat_file)
%        output_struct = load(data_mat_file);
%        file_dirs = output_struct.file_dirs;
%        labels = output_struct.labels;
%     else
%         % Caution: Assuming the root_dir ends with /
%         if strcmp(imset_options, 'all')
%             imset_list = {'train', 'test'};
%         else
%             imset_list = {imset_options};
%         end
% 
%         for k = 1:length(imset_list)
%           imset = imset_list{k};
%           content = fileread(fullfile(root_dir, sprintf('%s_rgb.txt', imset)));
%           content = regexp(content,'\n','split');
%           num_lines = size(content, 2)
% 
%           file_dirs = cell(1, num_lines);
%           labels = zeros(1, num_lines);
%           parfor i = 1:num_lines
%               raw_file_name = content{1, i};
%               if length(raw_file_name) == 0
%                   continue
%               end
%               file_name_start_ind = regexp(raw_file_name, '/rgb/', 'end') + 1;
%               file_name_end_ind= regexp(raw_file_name, '.png', 'start') - 1;
%               label_start_ind= regexp(raw_file_name, '.png', 'end') + 2;
%               file_name = raw_file_name(file_name_start_ind:file_name_end_ind);
%               file_dirs{1, i} = file_name;
%               labels(i) = str2num(raw_file_name(label_start_ind:end));
%           end
%         end
%         
%         save(data_mat_file, 'file_dirs', 'labels')
%         
%     end
  
end
  