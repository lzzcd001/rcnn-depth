function placeholder = collect_UW(jobDir)
  while ~collectJob(jobDir), pause(120); end
  placeholder = 0;
end