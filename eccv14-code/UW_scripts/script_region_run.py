import sys
import math
import matlab.engine
from multiprocessing import Pool

def runParallel(jobDir, ind_tuple, numjobs):
	eng = matlab.engine.start_matlab()
	eng.addpath(eng.genpath(eng.pwd()))
	for id_no in range(ind_tuple[0], ind_tuple[1]+1):
		eng.runJob(jobDir, id_no, numjobs)
	return ind_tuple[0]


eng = matlab.engine.start_matlab()
eng.addpath(eng.genpath(eng.pwd()))

assert len(sys.argv) == 3
jobName = sys.argv[1]
process_num = sys.argv[2]

jobDir, numjobs = eng.script_region_UW(jobName)
total_num = numjobs

assert process_num < total_num

pack_size = int(math.ceil(total_num / float(process_num)))
ind_tuple_list = []

for i in xrange(process_num):
	ind_tuple_list.append(
		(i*pack_size + 1, min((i+1)*pack_size - 1, total_num) )
		)
p = Pool(process_num)
print "Start"
p.map(lambda x: runParallel(jobDir, x, numjobs), ind_tuple_list)
eng.collect_UW(jobDir)

