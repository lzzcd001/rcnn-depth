function output = UW_background_extractor(start_ind, end_ind)

dataset_dir = '/home/tom/rgbd-scenes/background';
target_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_background';

if ~exist(target_dir)
    mkdir(target_dir)
end

class_folders = dir(dataset_dir);
class_folders = {class_folders.name};
class_folders = class_folders(3:end);
% class_folders

for folder_ind = start_ind:end_ind
    disp('current frame')
    disp(folder_ind)
    disp('current class')
    disp(class_folders{folder_ind})
    class_dir = fullfile(dataset_dir, class_folders{folder_ind});
    class_content_fnames = dir(class_dir);
    class_content_fnames = {class_content_fnames.name};
    class_content_fnames = class_content_fnames(3:end);
    for file_ind = 1:length(class_content_fnames)
        file_name = class_content_fnames{file_ind};
        start_pt = regexp(file_name, '_depth.png', 'start');
        if length(start_pt) == 0
            continue
        end
        rawdepth_name = [file_name(1:start_pt(1) - 1), '.png'];
        rawdepth_dir = fullfile(target_dir, class_folders{folder_ind}, 'rawdepth');
        if ~exist(rawdepth_dir)
            mkdir(rawdepth_dir)
        end
        if ~exist(fullfile(rawdepth_dir, rawdepth_name))
            copyfile(fullfile(class_dir, file_name), fullfile(rawdepth_dir, rawdepth_name));
        end
        
        rgb_name = [file_name(1:start_pt(1) - 1), '.png'];
        
        
        rgb_dir = fullfile(target_dir, class_folders{folder_ind}, 'rgb');
        if ~exist(rgb_dir)
            mkdir(rgb_dir)
        end
        if ~exist(fullfile(rgb_dir, rgb_name))
            copyfile(fullfile(class_dir, rgb_name), fullfile(rgb_dir, rgb_name));
        end
        
        
        
        depth_dir = fullfile(target_dir, class_folders{folder_ind}, 'depth');
        if ~exist(fullfile(depth_dir, rawdepth_name))
            RD = imread(fullfile(class_dir, file_name));
            rgb_img = imread(fullfile(class_dir, rgb_name));
            D = fill_depth_colorization(double(rgb_img) ./ 255, double(RD) ./ 1000);
            D = uint16(D * 1000);
            
            if ~exist(depth_dir)
                mkdir(depth_dir)
            end
            imwrite(D, fullfile(depth_dir, rawdepth_name));
        end
    end
end
    
    output = folder_ind;
end